import React, { useState, useEffect } from 'react'

const List = () => {
    
    const [ photos, setPhotos ] = useState([]);
   
    useEffect( () => {
        fetch(`https://jsonplaceholder.typicode.com/photos?_limit=5`)
            .then( res => res.json() )
            .then( photos => setPhotos( photos ))
            }, []);

    return(
        <div>
            <h5> Photos </h5>
            <ul>
                {
                    photos.length ?
                    photos.map(p => (
                        <li key={p.id}>
                            <a href={'/list/' + p.id}> {p.title} </a>
                        </li>
                    ))
                    :
                    <span>...Loading</span>
                }

            </ul>
        </div>
    );
    
    

};

export default List;
 