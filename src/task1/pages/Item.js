import React, { useState, useEffect } from 'react'
import NotFound from './NotFound'

const Item = ({ match }) => {

    const { itemid } = match.params;
    const [ photo, setPhoto ] = useState( null );
    
    useEffect(() => {
        fetch( `https://jsonplaceholder.typicode.com/photos/${itemid}`)
            .then( res => {
                if( res.status === 404 ){
                    throw 'Photo not found';
                }
                return  res.json();

            })
            .then( res => {
                setPhoto( res );
            })
            .catch( error => {
                console.log('error', error );
                setPhoto( undefined );
            })

        return () => {
            setPhoto( null );
        }

    }, [itemid] )    

    if( photo === undefined ){
        return <NotFound />
    }
    
    if( photo === null ){
        return(<> Loading </>)
    }
	
	return(
        <>
            <h5> Photo #{itemid}</h5> 
            Title: {photo.title} <br />
            <img src={photo.thumbnailUrl} alt={'photo '+ itemid}></img>

        </>

    );
};

export default Item;
 