import HomePage from "./task3/pages/HomePage";
import Posts from "./task3/pages/Posts";
import Post from "./task3/pages/Post";
import NotFound from "./task3/pages/NotFound";

export default [
    {
        type: 'route',
        exact: true,
        path: '/',
        to: '/',
        component: HomePage,
        title: "Home page"
    },    
    {
        type: 'route',
        exact: true,
        path: '/posts/limit/:postcount',
        to: '/posts/limit/7',
        component: Posts,
        title: "Posts"
    },
    {
        type: 'route',
        exact: true,
        path: '/posts/:postid',
        component: Post,
    },
    {
        type: 'route',
        exact: true,
        path: '/404',
        component: NotFound,
    },

];