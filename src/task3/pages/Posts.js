import React, { useState, useEffect } from 'react'

const Posts = ({match}) => {
    
    const { postcount } = match.params;
    const [ posts, setPosts ] = useState([]);
   
    useEffect( () => {
        fetch(`https://jsonplaceholder.typicode.com/posts?_limit=${postcount}`)
            .then( res => res.json() )
            .then( posts => setPosts( posts ))
            }, []);

    return(
        <div>
            <h5> Posts </h5>
            <ul>
                {
                    posts.length ?
                    posts.map(p => (
                        <li key={p.id}>
                            <a href={'/posts/' + p.id}> {p.title} </a>
                        </li>
                    ))
                    :
                    <span>...Loading</span>
                }
            </ul>
        </div>
    );
};

export default Posts; 