import React, { useState, useEffect } from 'react'
import NotFound from './NotFound'

const Post = ({ match }) => {

    const { postid } = match.params;
    const [ post, setPost ] = useState( null );
    const [ comments, setComments ] = useState( [] );
    const [ showcomments, setShowcomments ] = useState( false );
   
    useEffect(() => {
        fetch( `https://jsonplaceholder.typicode.com/posts/${postid}`)
            .then( res => {
                if( res.status === 404 ){
                    throw 'Post not found';
                }
                return  res.json();
            })
            .then( res => {
                setPost( res );
            })
            .catch( error => {
                console.log('error', error );
                setPost( undefined );
            })
        return () => {
            setPost( null );
        }

    }, [postid] )    

    useEffect( () => {
        fetch(`https://jsonplaceholder.typicode.com/posts/${postid}/comments`)
            .then( res => res.json() )
            .then( comments => setComments( comments ))
            }, []);

    if( post === undefined ){
        return <NotFound />
    }
    
    if( post === null ){
        return(<> Loading </>)
    }

    const ShowComments = () => {
        const onClick = () => setShowcomments(!showcomments)
        return(
            <div className="divPost">
            <button onClick={onClick}>Show comments...</button>
            { showcomments ? <Comments /> : null }
            </div>
        )
    }

    const Comments = () => {
        return(
        <div>
            <p /> Comments: 
            <ul>
            {
                comments.length ?
                comments.map(p => (
                    <li key={p.id}>
                        <b>{p.name}: </b> {p.body}
                    </li>
                ))
                :
                <span>...Loading</span>
            }
            </ul>
        </div>
      )
    }

	
	return(
        <>
            <p/> Post #{postid}
            <strong> {post.title} </strong>
            <div className="divPost"> {post.body} </div>
            <p />
            <ShowComments />
        </>

    );
};

export default Post;
 