import React, {useEffect, useState} from 'react'

const HomePage = () => {

    const [ posts, setPosts ] = useState([]);
    const [postcount, setPostcount] = useState(20);
    
   
    useEffect( () => {
        fetch(`https://jsonplaceholder.typicode.com/posts?_limit=${postcount}`)
            .then( res => res.json() )
            .then( posts => setPosts( posts ))
            });


    const Addpost = () => {
        setPostcount(postcount+20);
        console.log(postcount);
    }
    

    return(
        <div>
            <h5> Posts </h5>
            <ol>
                {
                    posts.length ?
                    posts.map(p => (
                        <li key={p.id}>
                            <a href={'/posts/' + p.id}> {p.title} </a>
                        </li>
                    ))
                    :
                    <span>...Loading</span>
                }
            </ol>
        <button onClick={Addpost}>Show more...</button>
        </div>
    );



};

export default HomePage;
 