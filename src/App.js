import React, {Component} from 'react'
import { Switch, Route, Redirect, NavLink, BrowserRouter } from 'react-router-dom';
import routes_map from './routes_map';
import routes_map_post from './routes_map_post';
import './App.css';

class App extends Component {

  render() {

    const links = routes_map.filter( route => route.title !== undefined );

    const links_post = routes_map_post.filter( route => route.title !== undefined );

    return (
      
    <div className="App">

      <h3>Task 1-2</h3>

      <BrowserRouter> 
        <div>
        {
          links.map( link => {
            return(
              <NavLink key={link.title} to={link.path}>
               { link.title } &nbsp;&nbsp;
              </NavLink>
            )
          })
        }
        </div>
        <Switch>
            {
              routes_map.map( (route, index) => {
                if( route.type === "redirect"){
                  return <Redirect key={index} {...route} />
                }
                if( route.type === "route"){
                  return <Route key={index} {...route} />
                }
              })
            }
        </Switch>
      </BrowserRouter>

      <h3>Task 3</h3>
      <BrowserRouter> 
        <div>
        {
          links_post.map( link => {
            return(
              <NavLink key={link.title} to={link.to}>
               { link.title } &nbsp;&nbsp;

              </NavLink>
            )
          })
        }
        </div>
        <Switch>
            {
              routes_map_post.map( (route, index) => {
                if( route.type === "redirect"){
                  return <Redirect key={index} {...route} />
                }
                if( route.type === "route"){
                  return <Route key={index} {...route} />
                }
              })
            }
        </Switch>
      </BrowserRouter>


  	</div>

    )
  }
}


export default App; 
