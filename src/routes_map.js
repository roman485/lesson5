import HomePage from "./task1/pages/HomePage";
import About from "./task1/pages/About";
import Contacts from "./task1/pages/Contacts";
import List from "./task1/pages/List";
import Item from "./task1/pages/Item";
import NotFound from "./task1/pages/NotFound";

export default [
    {
        type: 'route',
        exact: true,
        path: '/',
        component: HomePage,
        title: "Home page"
    },    
    {
        type: 'route',
        exact: true,
        path: '/List',
        component: List,
        title: "List"
    },
    {
        type: 'route',
        exact: true,
        path: '/list/:itemid',
        component: Item,
    },
    {
        type: 'route',
        exact: true,
        path: '/about',
        component: About,
        title: "About"
    },
    {
        type: 'route',
        exact: true,
        path: '/contacts',
        component: Contacts,
        title: "Contacts"
    },
    {
        type: 'route',
        exact: true,
        path: '/404',
        component: NotFound,
    }

];